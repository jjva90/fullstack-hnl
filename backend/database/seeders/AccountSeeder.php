<?php

namespace Database\Seeders;

use App\Models\Account;
use App\Models\Employee;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getAccount() as $index => $accountDetails) {
            Account::firstOrCreate([       
               "number" => $accountDetails->number,
               "description" => $accountDetails->description,
               "total" => $accountDetails->total,
               "period_start" => $accountDetails->period_start,
               "period_end" => $accountDetails->period_end,
               "concept_id" => $accountDetails->concept_id,
               "employee_id" => $accountDetails->employee_id
            ]);            
        }
    }

    private function getAccount() {
        $names = ["Loyal Sunshine", "Yellow Trader", "Calm Tailor", "Icy Champion"];
        $description = ["Trabajo", "Requerimientos", "Materiales", "Cosas"];
        $accountNumbers = [79655,78954,69874,23541];
        $accounts = [];
        for ($i=0; $i < count($names); $i++) { 
            $period_start = Carbon::today()->subDays(rand(0, 365));
            $period_end = $period_start->copy()->addDays(rand(1, 31));

            $accountDetails = new \stdClass;
            $accountDetails->number = $accountNumbers[$i];
            $accountDetails->description = $description[array_rand($description)];            
            $accountDetails->total =rand(0, 9999);
            $accountDetails->period_start = $period_start; 
            $accountDetails->period_end = $period_end;            
            $accountDetails->concept_id = rand(1, 3);
            $accountDetails->employee_id = Employee::where('name', $names[$i])->first()->id;
            array_push($accounts, $accountDetails);
        }
        return $accounts;
    }
}

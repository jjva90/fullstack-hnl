<?php

namespace Database\Seeders;

use App\Models\Manager;
use Illuminate\Database\Seeder;

class ManagerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getManagers() as $index => $manager) {
            Manager::firstOrCreate([
                'name' => $manager
            ]);
        }
    }

    private function getManagers() {
        return $managers = ["Fresh Supernova","Huge Rainbow","Enchanted Salesman"];
    }
}

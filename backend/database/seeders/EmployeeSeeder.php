<?php

namespace Database\Seeders;

use App\Models\Manager;
use App\Models\Employee;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{   
    const POSITIONS = [
        "Vendedor",
        "Desarrollador",
        "Tester",
        "Publicista"
    ];
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getEmployees() as $index => $employeeDetails) {
            $department = $manager = "";
            if ($employeeDetails->position == "Vendedor") {
                $manager = Manager::firstOrCreate([
                    "name" => "Enchanted Salesman"
                ]);
                $department = "Ventas";                
            } elseif  ($employeeDetails->position == "Desarrollador" || $employeeDetails->position ==  "Tester") {
                $manager = Manager::firstOrCreate([
                    "name" => "Fresh Supernova"
                ]);
                $department = "IT";   
            } elseif  ($employeeDetails->position == "Publicista") {
                $manager = Manager::firstOrCreate([
                    "name" => "Huge Rainbow"
                ]);
                $department = "Mercadeo";   
            }
            if ($manager) {
                Employee::firstOrCreate([
                    "name" => $employeeDetails->name,
                    "position" => $employeeDetails->position,
                    "department" => $department,
                    "manager_id" => $manager->id
                ]);
            }
            
        }
    }

    private function getEmployees() {
        $names = ["Loyal Sunshine", "Yellow Trader", "Calm Tailor", "Icy Champion"];
        $employees = [];
        for ($i=0; $i < count($names); $i++) { 
            $employee = new \stdClass;
            $employee->name = $names[$i];
            $employee->position = self::POSITIONS[array_rand(self::POSITIONS)];
            array_push($employees, $employee);
        }
        return $employees;
    }
}

<?php

namespace Database\Seeders;

use App\Models\Concept;
use Illuminate\Database\Seeder;

class ConceptSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getConcepts() as $index => $concept) {
            Concept::firstOrCreate([
                'name' => $concept
            ]);
        }
    }

    private function getConcepts() {
        return $concepts = ["Compras","Negocios","Reuniones"];
    }
}

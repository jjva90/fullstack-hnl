import React, { Component } from "react";
import axios from "axios";
import Home from "../components/Home";

class HomeContainer extends Component {
  state = {
    reporte: [],
  };

  componentDidMount() {
    axios
      .post("http://127.0.0.1:8000/api/employee-report", {
        conceptId: 2,
        periodStart: "2020-12-27",
        periodEnd: "2021-01-13",
        employeeId: 2,
      })
      .then(
        (response) => {
          const reports = response.data;
          this.setState({
            reporte: reports,
          });
        },
        (error) => {
          console.log(error);
        }
      );
  }

  render() {
    const { reporte } = this.state;

    return <Home reportes={reporte} />;
  }
}

export default HomeContainer;

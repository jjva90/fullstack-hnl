<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('number');
            $table->string('description');
            $table->decimal('total', 8, 2);
            $table->date('period_start');
            $table->date('period_end');
            $table->unsignedBigInteger('concept_id');
            $table->unsignedBigInteger('employee_id');
            $table->timestamps();

            $table->foreign('concept_id')->references('id')->on('concepts');
            $table->foreign('employee_id')->references('id')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}

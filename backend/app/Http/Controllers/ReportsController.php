<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Concept;
use App\Models\Employee;
use Illuminate\Http\Request;

class ReportsController extends Controller
{
    public function index()
    {
        $response = [];
        $concepts = Concept::all();
        $employees = Employee::all();
        $response['data'] = [
            "reports" => $concepts,
            "employees" => $employees
        ];
        return response()->json($response, 200);
    }

    public function getEmployee()
    {
        return Employee::all();
    }

    public function getEmployeeReport(Request $request)
    {
        $response = [];
        $conceptId = $request->input("conceptId");
        $periodStart = $request->input("periodStart");
        $periodEnd = $request->input("periodEnd");
        $employeeId = $request->input("employeeId");

        $reports = Account::where('concept_id', $conceptId)
            ->where('period_start', $periodStart)
            ->where('period_end', $periodEnd)
            ->where('employee_id', $employeeId)
            ->get();
        $response['data'] = [
            "reports" => $reports
        ];
        return response()->json($response, 200);
    }
}

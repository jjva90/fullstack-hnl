# Iniciando la API en Laravel (Backend)

`php artisan migrate`

## Seeders de prueba de la base de datos

```
php artisan db:seed --class=ConceptSeeder
php artisan db:seed --class=ManagerSeeder
php artisan db:seed --class=EmployeeSeeder
php artisan db:seed --class=AccountSeeder
```


# ENDPOINTS (Frontend)

Para consultar los conceptos de los gastos y la lista de empleados empleados

**GET:** `/api/concept`

Una vez tengamos el concepto, el empleado y el periodo se puede consultar los gastos que se registraron

**POST:** `/api/employee-report`

Parametros:
```
conceptId
periodStart
periodEnd
employeeId
```

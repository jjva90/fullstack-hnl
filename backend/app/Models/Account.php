<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable =[
        'number',
        'description',
        'total',
        'period_start',
        'period_end',
        'concept_id',
        'employee_id'
    ];
}
